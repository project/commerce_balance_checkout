<?php

/**
 * @file
 * Provide views data for the Commerce balance checkout module.
 */

/**
 * Implements hook_views_data().
 */
function commerce_balance_checkout_views_data() {
  $data['views']['commerce_balance_checkout_balance_total'] = [
    'title' => t('Balance total'),
    'help' => t('Displays the order total field, requires an Order ID argument.'),
    'area' => [
      'id' => 'commerce_balance_checkout_balance_total',
    ],
  ];
  return $data;
}
