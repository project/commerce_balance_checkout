<?php

declare(strict_types = 1);

namespace Drupal\commerce_balance_checkout\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the balance checkout redirect form.
 */
class BalanceCheckoutRedirectForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new BalanceCheckoutRedirectForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'balance_checkout_redirect_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['redirect'] = [
      '#type' => 'submit',
      '#value' => $this->t('Proceed to balance payment'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): mixed {
    if (!$order_id = $form_state->get('order_id')) {
      return parent::submitForm($form, $form_state);
    }

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->entityTypeManager
      ->getStorage('commerce_order')
      ->load($order_id);

    if ($order->hasField('checkout_flow')) {
      $order->set('checkout_flow', NULL);
    }
    if ($order->hasField('checkout_step')) {
      $order->set('checkout_step', '');
    }
    $order->save();

    $path = Url::fromRoute(
      'commerce_checkout.form',
      ['commerce_order' => $order->id()]
    )->toString();
    $response = new RedirectResponse($path);
    $response->send();
  }

}
