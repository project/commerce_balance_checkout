<?php

declare(strict_types = 1);

namespace Drupal\commerce_balance_checkout\Plugin\Commerce\CheckoutFlow;

use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowWithPanesBase;

/**
 * Provides the balance multistep checkout flow.
 *
 * @CommerceCheckoutFlow(
 *  id = "commerce_balance_checkout_balance_multistep",
 *  label = @Translation("Multistep - Balance"),
 * )
 */
class BalanceMultistep extends CheckoutFlowWithPanesBase {

  /**
   * {@inheritdoc}
   */
  public function getSteps(): array {
    return [
      'payment_information' => [
        'label' => $this->t('Balance payment information'),
        'has_sidebar' => TRUE,
        'next_label' => $this->t('Continue and pay the order balance'),
      ],
    ] + parent::getSteps();
  }

  /**
   * {@inheritdoc}
   */
  public function getStepId($requested_step_id = NULL): string|null {
    if ($this->order->isPaid()) {
      return 'complete';
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $conf = $form_state->getValue('configuration');
    if ($conf['panes']['coupon_redemption']['step_id'] !== '_disabled') {
      $form_state->setError($form['panes']['coupon_redemption'], $this->t('Coupon redemption pane cannot be enabled on balance flows.'));
    }
  }

}
