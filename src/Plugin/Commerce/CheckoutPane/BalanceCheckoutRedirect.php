<?php

declare(strict_types = 1);

namespace Drupal\commerce_balance_checkout\Plugin\Commerce\CheckoutPane;

use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;

/**
 * Provides the balance checkout redirect pane.
 *
 * @CommerceCheckoutPane(
 *   id = "balance_checkout_redirect",
 *   label = @Translation("Balance checkout redirect"),
 *   default_step = "complete",
 * )
 */
class BalanceCheckoutRedirect extends CheckoutPaneBase {

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form): array {
    $pane_form['redirect'] = [
      '#type' => 'submit',
      '#value' => $this->t('Proceed to balance payment'),
    ];

    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form): void {
    if ($this->order->hasField('checkout_flow')) {
      $this->order->set('checkout_flow', NULL);
    }

    if ($this->order->hasField('checkout_step')) {
      $this->order->set('checkout_step', '');
    }

    $path = Url::fromRoute(
      'commerce_checkout.form',
      ['commerce_order' => $this->order->id()]
    )->toString();
    $response = new RedirectResponse($path);
    $response->send();
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible(): bool {
    return !$this->order->isPaid();
  }

}
