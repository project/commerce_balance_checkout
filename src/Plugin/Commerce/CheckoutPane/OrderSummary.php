<?php

declare(strict_types=1);

namespace Drupal\commerce_balance_checkout\Plugin\Commerce\CheckoutPane;

use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_balance_checkout\Plugin\Commerce\CheckoutFlow\BalanceMultistep;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\OrderSummary as OrderSummaryBase;

/**
 * Provides the Order summary pane.
 *
 * @CommerceCheckoutPane(
 *   id = "order_summary",
 *   label = @Translation("Order summary"),
 *   default_step = "_sidebar",
 *   wrapper_element = "container",
 * )
 */
class OrderSummary extends OrderSummaryBase {

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form): array {
    $pane_form = parent::buildPaneForm($pane_form, $form_state, $complete_form);

    if (($form_state->getFormObject() instanceof BalanceMultistep) && !$this->configuration['view']) {
      $pane_form['summary']['#theme'] = 'commerce_balance_checkout_order_summary';
    }

    return $pane_form;
  }

}
