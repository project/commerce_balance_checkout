<?php

namespace Drupal\commerce_balance_checkout\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\commerce_order\Plugin\Field\FieldFormatter\OrderTotalSummary;

/**
 * Plugin implementation of the 'commerce_balance_checkout_total_summary' formatter.
 *
 * @FieldFormatter(
 *   id = "commerce_balance_checkout_total_summary",
 *   label = @Translation("Balance total summary"),
 *   field_types = {
 *     "commerce_price",
 *   },
 * )
 */
class BalanceTotalSummary extends OrderTotalSummary {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $items->getEntity();
    $elements = [];
    if (!$items->isEmpty()) {
      $elements[0] = [
        '#theme' => 'commerce_balance_checkout_total_summary',
        '#order_entity' => $order,
        '#totals' => $this->orderTotalSummary->buildTotals($order) + [
          'totalpaid' => $order->getTotalPaid(),
          'balance' => $order->getBalance(),
        ],
      ];
    }

    return $elements;
  }

}
