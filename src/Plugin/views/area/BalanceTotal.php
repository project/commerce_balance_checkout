<?php

namespace Drupal\commerce_balance_checkout\Plugin\views\area;

use Drupal\commerce_order\Plugin\views\area\OrderTotal;
use Drupal\views\Plugin\views\argument\NumericArgument;

/**
 * Defines an order balance total area handler.
 *
 * Shows the order balance total field with its components listed in the
 * footer of a View.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("commerce_balance_checkout_balance_total")
 */
class BalanceTotal extends OrderTotal {

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    if (!$empty || !empty($this->options['empty'])) {
      foreach ($this->view->argument as $name => $argument) {
        // First look for an order_id argument.
        if (!$argument instanceof NumericArgument) {
          continue;
        }
        if (!in_array($argument->getField(), [
          'commerce_order.order_id',
          'commerce_order_item.order_id',
          'commerce_payment.order_id',
        ])) {
          continue;
        }
        /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
        if ($order = $this->orderStorage->load($argument->getValue())) {
          $order_total = $order->get('total_price')->view([
            'label' => 'hidden',
            'type' => 'commerce_balance_checkout_total_summary',
            'weight' => $this->position,
          ]);
          $order_total['#prefix'] = '<div data-drupal-selector="order-total-summary">';
          $order_total['#suffix'] = '</div>';
          return $order_total;
        }
      }
    }
    return [];
  }

}
