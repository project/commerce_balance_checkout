<?php

declare(strict_types=1);

namespace Drupal\commerce_balance_checkout\Resolver;

use Drupal\commerce_checkout\Entity\CheckoutFlowInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_checkout\Resolver\CheckoutFlowResolverInterface;

/**
 * Returns the order type's default or the balance checkout flow.
 */
class CheckoutFlowResolver implements CheckoutFlowResolverInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new DefaultCheckoutFlowResolver object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function resolve(OrderInterface $order): CheckoutFlowInterface {
    $checkout_flow_storage = $this->entityTypeManager->getStorage('commerce_checkout_flow');

    if ($order->getState()->getId() != 'draft' && !$order->isPaid()) {
      $checkout_flow = $checkout_flow_storage->load('balance');
      return $checkout_flow;
    }
    else {
      /** @var \Drupal\commerce_order\Entity\OrderTypeInterface $order_type */
      $order_type = $this->entityTypeManager->getStorage('commerce_order_type')->load($order->bundle());
      $checkout_flow_id = $order_type->getThirdPartySetting('commerce_checkout', 'checkout_flow', 'default');
      $checkout_flow = $checkout_flow_storage->load($checkout_flow_id);
      return $checkout_flow;
    }

  }

}
