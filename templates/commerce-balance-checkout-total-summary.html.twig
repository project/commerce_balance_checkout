{#
/**
 * @file
 * Balance order total summary template.
 *
 * Available variables:
 * - attributes: HTML attributes for the wrapper.
 * - order_entity: The order entity.
 * - totals: An array of order totals values with the following keys:
 *   - subtotal: The order subtotal price.
 *   - adjustments: The adjustments:
 *     - type: The adjustment type.
 *     - label: The adjustment label.
 *     - amount: The adjustment amount.
 *     - percentage: The decimal adjustment percentage, when available. For example, "0.2" for a 20% adjustment.
 *   - total: The order total price.
 *   - totalpaid: The order total paid.
 *   - balance: The order balance.
 *
 * @ingroup themeable
 */
#}

{{ attach_library('commerce_balance_checkout/total_summary') }}
<div{{ attributes }}>
  {% if totals.subtotal %}
    <div class="order-total-line order-total-line__subtotal">
      <span class="order-total-line-label">{{ 'Subtotal'|t }} </span><span class="order-total-line-value">{{ totals.subtotal|commerce_price_format }}</span>
    </div>
  {% endif %}
  {% for adjustment in totals.adjustments %}
    <div class="order-total-line order-total-line__adjustment order-total-line__adjustment--{{ adjustment.type|clean_class }}">
      <span class="order-total-line-label">{{ adjustment.label }} </span><span class="order-total-line-value">{{ adjustment.amount|commerce_price_format }}</span>
    </div>
  {% endfor %}
  {% if totals.total %}
    <div class="order-total-line order-total-line__total">
      <span class="order-total-line-label">{{ 'Total'|t }} </span><span class="order-total-line-value">{{ totals.total|commerce_price_format }}</span>
    </div>
  {% endif %}
  {% if totals.totalpaid %}
    <div class="order-total-line order-total-line__totalpaid">
      <span class="order-total-line-label">{{ 'Already paid' }} </span><span class="order-total-line-value">{{ totals.totalpaid|commerce_price_format }}</span>
    </div>
  {% endif %}
  {% if totals.balance %}
    <div class="order-total-line order-total-line__balance">
      <span class="order-total-line-label">{{ 'Left to pay'|t }} </span><span class="order-total-line-value">{{ totals.balance|commerce_price_format }}</span>
    </div>
  {% endif %}
</div>
